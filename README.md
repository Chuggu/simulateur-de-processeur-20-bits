# Simulateur de processeur 20-bits

Pour la réalisation du projet "Simulateur de processeur 20-bits", les outils que j'ai utilisé sont :

* __Visual Studio Code__ pour la rédaction du code (en C)
* Le compilateur __gcc__
* Un __Makefile__ (les commandes sont 'make compile' pour compiler et 'make' pour exécuter)
* __Gitlab__ (le lien pour retrouver mon projet est : https://gitlab.com/Chuggu/simulateur-de-processeur-20-bits.git/ )


Pour le code en lui même, j'ai choisi d'utiliser une fonction 'principale' récursive qui suit l'algorithme de la figure 1.8 du sujet. Cette fonction fait elle même appel à d'autres fonctions pour certaines des instructions IAS.

J'ai aussi pris le parti de convertir, grâce à une fonction dédiée, les instructions, adresses et valeurs en entiers décimaux pour en facilité le traitement par la suite (par exemple pour le switch case qui permet de déterminer à quelle instruction nous avons à faire).