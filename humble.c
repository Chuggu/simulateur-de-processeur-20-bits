#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <math.h>


int BtoD(int tab[], int m, int n)
{
    int entier = 0, i, j=0;

    for(i=n-1;i>m;i--)
    {
        entier = entier + tab[i]*pow(2,j);
        j++;
    }
    if(n==40 && tab[0]==0)
    {
        entier = entier*(-1);
    }
    return entier;
}

//======================================================

int loadMX(int mar)      
{
    int valeur, adresse, i, c, j;
    int t[40];
    FILE* fic = NULL;
    fic = fopen("test.txt","r");

    if (fic == NULL)
    {
        printf("Impossible d'ouvrir le fichier \n");
        exit(0);
    }

    adresse = mar;

    i = (adresse*40)+adresse;

    fseek(fic,i,SEEK_SET);

    for(j=0; j<40;j++)
    {
        c = fgetc(fic);
        if(c == EOF)exit(0);       
        t[j] = c-48;
    }
    valeur = BtoD(t,0,40);

    fclose(fic);

    return valeur;
}
//==========================================================

/*void wfic2(int tab2D[][40], int increment)
{
    
    FILE* fichier2 = NULL;
    fichier2 = fopen("test2.txt","w");

    if (fichier2 == NULL)
    {
        printf("Impossible d'ouvrir le fichier \n");
        exit(0);
    }

    //printf("Nous sommes actuellement dans le trouble \n");
    //printf("increment = %d\n\n",increment);

    for(int i=0;i<40;i++)
    {
        //printf("coco");
        fprintf(fichier2,"%d",tab2D[increment][i]);
    }
    fprintf(fichier2,"\n");
    
    fclose(fichier2);
}*/

//============================================================

/*void loadMem()
{
    int i,j,c;
    int tab2D[4096][40];

    FILE* fichier = NULL;
    fichier = fopen("test.txt","r");

    if (fichier == NULL)
    {
        printf("Impossible d'ouvrir le fichier \n");
        exit(0);
    }

    j=0;
    while(c!=EOF)
    {
        printf("%d  -  ",j);
        for(i = 0; i<40;i++)
        {
            c = fgetc(fichier);
            tab2D[j][i] = c-48;
            printf("%d",tab2D[j][i]);         
        }
        fseek(fichier,1,SEEK_CUR);
        printf("\n");
        j++;
    }

    fclose(fichier);

}*/

//============================================================

void printMem(int tab2D[4096][40],int size)
{
    int i, j;
    printf("\t\t   MEMORIE MAP\n");
    printf("---------------------------------------------------\n");

    for(int j=0;j<size-1;j++)
    {
        if(j<10)
        {
            printf("|  %d  -  ",j);
        }
        else
        {
            printf("| %d  -  ",j);
        }
        for(i = 0; i<40;i++)
        {
            printf("%d",tab2D[j][i]);
        }
        printf(" |");
        printf("\n");
    }

    printf("---------------------------------------------------\n\n");

}
//============================================================

void stor(int ac, int mar, int tab2D[4096][40])
{
    int i,j,ad,c,k = 39;
    int t[40] = {0};

    ad = abs(ac);

    for(j = 0; ad > 0; j++)
    {
        t[j] = ad % 2;
        ad = ad / 2;
    }

    for(i=0; i<40; i++)
    {
        tab2D[mar][k] = t[i];
        k=k-1;
    }
    if(ac<0)
        tab2D[mar][0] = 0;
    else 
        tab2D[mar][0] = 1;

}

//==================================================================

void storAdd(int ac, int mar, int tab2D[4096][40], int k)
{
    int i,j,ad,c;
    int t[12] = {0};

    ad = abs(ac);

    for(j = 0; ad > 0; j++)
    {
        t[j] = ad % 2;
        ad = ad / 2;
    }

    for(i=0; i<12; i++)
    {
        tab2D[mar][k] = t[i];
        k=k-1;
    }

}

//======================================================

void soloQ(int ibr, int pc, int ac, int mq, int t2d[4096][40], int size)
{
    int i, c, curseur, valeur, mar, mbr, ir, it; 
    int op1, add1, op2, add2;
    int tab[40];

    FILE* fichier = NULL;
    fichier = fopen("test.txt","r");

    if (fichier == NULL)
    {
        printf("Impossible d'ouvrir le fichier \n");
        exit(0);
    }


    curseur = ((pc)*40)+(pc);
    fseek(fichier,curseur,SEEK_SET);

    for(i = 0; i<40;i++)
    {
        c = fgetc(fichier);
        if(c==EOF)exit(0);
        tab[i] = c-48;        
    }

    printf("\nPC  : %d\n",pc);
    printf("IBR : %d\n",ibr);


    op1 = abs(BtoD(tab,0,8));
    add1 = abs(BtoD(tab,8,20));
    op2 = abs(BtoD(tab,20,28));
    add2 = abs(BtoD(tab,28,40));

    if(ibr == 0) // premier NO
    {
        mar = pc;
        printf("MAR : %d\n",mar);

        mbr = add2 + (op2*10) + (add1*100) + (op1*1000);
        printf("MBR : %d\n",mbr);

        ibr = op2*10 + add2;
        printf("IBR : %d\n",ibr);
        ir = op1;
        printf("IR  : %d\n",ir);
        mar = add1;
        printf("MAR : %d\n",mar);
    }
    else        // premier YES
    {
        mbr = add2 + (op2*10) + (add1*100) + (op1*1000);
        printf("MBR : %d\n",mbr);
        
        ir = op2;
        printf("IR  : %d\n",ir);
        mar = add2;
        printf("MAR : %d\n",mar);
        pc = pc + 1;
        printf("PC  : %d\n",pc);
        ibr = 0;
        printf("IBR : %d\n",ibr);
    }

    switch (ir)    
    {
    case 1:
        //LOAD M(X)
        mbr = loadMX(mar);
        printf("MBR : %d\n",mbr);
        ac = mbr;
        printf("AC  : %d\n\n",ac);
        break;
    
    case 2:
        //LOAD -M(X)
        mbr = loadMX(mar);
        printf("MBR : %d\n",mbr);
        ac = (-1) * mbr;
        printf("AC  : %d\n\n",ac);
        break;

    case 3:
        //LOAD |M(X)|
        mbr = loadMX(mar);
        printf("MBR : %d\n",mbr);
        ac = abs(mbr);
        printf("AC  : %d\n\n",ac);
        break;

    case 4:
        //LOAD -|M(X)|
        mbr = loadMX(mar);
        printf("MBR : %d\n",mbr);
        ac = (-1) * abs(mbr);
        printf("AC  : %d\n\n",ac);
        break;

    case 5:
        //ADD M(X)
        mbr = loadMX(mar);
        printf("MBR : %d\n",mbr);
        ac = ac + mbr;
        printf("AC  : %d\n\n",ac);
        break;

    case 6:
        //SUB M(X)
        mbr = loadMX(mar);
        printf("MBR : %d\n",mbr);
        ac = ac - mbr;
        printf("AC  : %d\n\n",ac);
        break;

    case 7:
        //ADD |M(X)|
        mbr = loadMX(mar);
        printf("MBR : %d\n",mbr);
        ac = ac + abs(mbr);
        printf("AC  : %d\n\n",ac);
        break;

    case 8:
        //SUB |M(X)|
        mbr = loadMX(mar);
        printf("MBR : %d\n",mbr);
        ac = ac - abs(mbr);
        printf("AC  : %d\n\n",ac);
        break;

    case 9:
        //LOAD MQ,M(X)
        mbr = loadMX(mar);
        printf("MBR : %d\n",mbr);
        mq = mbr;
        printf("MQ  : %d\n\n",mq);
        break;

    case 10:
        //LOAD MQ
        ac = mq;
        printf("AC  : %d\n",ac);
        mq = 0;
        printf("MQ  : %d\n\n",mq);
        break;

    case 11:
        //MUL M(X)
        mbr = loadMX(mar);
        printf("MBR : %d\n",mbr);
        printf("MQ  : %d\n",mq);
        ac = mq * mbr;
        printf("AC  : %d\n\n",ac);
        break;

    case 12:
        //DIV M(X)
        mbr = loadMX(mar);
        printf("MBR : %d\n",mbr);
        printf("AC  : %d\n",ac);
        mq = ac / mbr;
        printf("MQ  : %d\n",mq);
        ac = ac % mbr;
        printf("AC  : %d\n\n",ac);
        break;

    case 13:
        //JUMP M(X,0:19)
        ibr = 0;
        pc = mar;
        printf("PC  : %d\n\n", pc);
        break;

    case 14:
        //JUMP M(X,20:39)
        ibr = 1;
        pc = mar;
        printf("PC  : %d\n\n", pc);
        break;

    case 15:
        //JUMP+ M(X,0:19)
        if(ac<0)
        {
        ibr = 0;
        pc = mar;
        printf("PC  : %d\n\n", pc);
        }
        break;

    case 16:
        //JUMP+ M(X,20:39)
        if(ac<0)
        {
        ibr = 1;
        pc = mar;
        printf("PC  : %d\n\n", pc);
        }
        break;

    case 18:
        //STOR M(X,8:19)
        storAdd(ac, mar, t2d, 19);
        break;

    case 19:
        //STOR M(X,28:39)
        storAdd(ac, mar, t2d, 39);
        break;

    case 20:
        //LSH
        ac = ac * 2;
        printf("AC  : %d\n\n",ac);
        break;

    case 21:
        //RSH
        ac = ac / 2;
        printf("AC  : %d\n\n",ac);
        break;

    case 33:
        //STOR M(X)
        //printf("STOR M(X)\n");
        stor(ac,mar,t2d);
        break;

    default:
        //valeur
        valeur = BtoD(tab,0,40);
        printf("Valeur : %d\n\n", valeur);  
        break;
    }

    fclose(fichier);

    printMem(t2d,size);
    soloQ(ibr,pc, ac, mq,t2d,size);
}

int main()
{
    int ibr = 0, pc = 0, ac = 0, mq = 0;
    int tab2D[4096][40];
    int c,i,j=0;

    FILE* fichier = NULL;
    fichier = fopen("test.txt","r");

    if (fichier == NULL)
    {
        printf("Impossible d'ouvrir le fichier \n");
        exit(0);
    }

    while(c!=EOF)
    {
        for(i = 0; i<40;i++)
        {
            c = fgetc(fichier);
            tab2D[j][i] = c-48;      
        }
        fseek(fichier,1,SEEK_CUR);
        j++;
    }
    
    soloQ(ibr,pc, ac, mq,tab2D,j);

    return 0;
}