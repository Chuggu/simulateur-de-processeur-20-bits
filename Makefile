execute:simul
	./simul


compile: fonctions.o main.o
	gcc -o simul fonctions.o main.o -lm -w
	

fonctions.o: fonctions.c
	gcc -o fonctions.o -c fonctions.c -Wall -O

main.o: main.c fonctions.h
	gcc -o main.o -c main.c -Wall -O
