#include "fonctions.h"

//=======================================================================
//  Fonction qui permet de passer d'un nombre binaire à un nombre décimal
//=======================================================================
int BtoD(int tab[], int m, int n)
{
    int entier = 0, i, j=0;

    for(i=n-1;i>m;i--)
    {
        entier = entier + tab[i]*pow(2,j);
        j++;
    }
    if(n==40 && tab[0]==0)
    {
        entier = entier*(-1);
    }
    return entier;
}

//==============================================================
//  Fonction utilisé pour de nombreuse instructions qui récupère
//  une valeur à une adresse donnée dans le fichier test.txt, 
//  la transforme en décimal et la retourne.
//==============================================================

int loadMX(int mar)      
{
    int valeur, adresse, i, c, j;
    int t[40];
    FILE* fic = NULL;
    fic = fopen("test.txt","r");

    if (fic == NULL)
    {
        printf("Impossible d'ouvrir le fichier \n");
        exit(0);
    }

    adresse = mar;

    i = (adresse*40)+adresse;

    fseek(fic,i,SEEK_SET);

    for(j=0; j<40;j++)
    {
        c = fgetc(fic);
        if(c == EOF)exit(0);       
        t[j] = c-48;
    }
    valeur = BtoD(t,0,40);

    fclose(fic);

    return valeur;
}

//====================================================================================
//  Fonction qui permet d'afficher le tableau représentant la mémoire dans le terminal
//====================================================================================

void printMem(int tab2D[4096][40],int size)
{
    int i, j;
    printf("\t\t   MEMORIE MAP\n");
    printf("+-------------------------------------------------+\n");

    for(j=0;j<size-1;j++)
    {
        if(j<10)
        {
            printf("|  %d  -  ",j);
        }
        else
        {
            printf("| %d  -  ",j);
        }
        for(i = 0; i<40;i++)
        {
            printf("%d",tab2D[j][i]);
        }
        printf(" |");
        printf("\n");
    }

    printf("+-------------------------------------------------+\n\n");

}
//==================================================
//  Fonction utilisée pour l'instruction STOR M(X)
//  Elle permet de passer d'un nombre décimal à un 
//  nombre binaire sur 40 bits et de le stocker dans
//  la mémoire à l'adresse indiquée
//==================================================
void stor(int ac, int mar, int tab2D[4096][40])
{
    int i,j,ad,k = 39;
    int t[40] = {0};

    ad = abs(ac);

    for(j = 0; ad > 0; j++)
    {
        t[j] = ad % 2;
        ad = ad / 2;
    }

    for(i=0; i<40; i++)
    {
        tab2D[mar][k] = t[i];
        k=k-1;
    }
    if(ac<0)
        tab2D[mar][0] = 0;
    else 
        tab2D[mar][0] = 1;

}

//===========================================================================
//  Fonction utilisée pour les instructions STOR M(X,8:19) et STOR M(X,28:39)
//===========================================================================

void storAdd(int ac, int mar, int tab2D[4096][40], int k)
{
    int i,j,ad;
    int t[12] = {0};

    ad = abs(ac);

    for(j = 0; ad > 0; j++)
    {
        t[j] = ad % 2;
        ad = ad / 2;
    }

    for(i=0; i<12; i++)
    {
        tab2D[mar][k] = t[i];
        k=k-1;
    }

}

//===============================
// Fonction récursive principale
//===============================

void cycleRec(int ibr, int pc, int ac, int mq, int t2d[4096][40], int size)
{
    int i, c, curseur, valeur, mar, mbr, ir; 
    int op1, add1, op2, add2;
    int tab[40];

    FILE* fichier = NULL;
    fichier = fopen("test.txt","r");        //Ouverture du fichier test.txt

    if (fichier == NULL)
    {
        printf("Impossible d'ouvrir le fichier \n");
        exit(0);
    }


    curseur = ((pc)*40)+(pc);
    fseek(fichier,curseur,SEEK_SET);    //Placement du curseur à la ligne correspondante au pc

    for(i = 0; i<40;i++)            //Lecture d'une ligne du fichier test.txt
    {
        c = fgetc(fichier);
        if(c==EOF)exit(0);
        tab[i] = c-48;        
    }

    printf("\nPC  : %d\n",pc);
    printf("IBR : %d\n",ibr);


    op1 = abs(BtoD(tab,0,8));       //Opcode de la première instruction en décimal
    add1 = abs(BtoD(tab,8,20));     //Adresse de la première instruction en décimal
    op2 = abs(BtoD(tab,20,28));     //Opcode de la deuxième instruction en décimal
    add2 = abs(BtoD(tab,28,40));    //Adresse de la deuxième instruction en décimal

    if(ibr == 0)            //Nous sommes dans la première instruction de l'adresse mémoire
    {
        mar = pc;
        printf("MAR : %d\n",mar);

        mbr = add2 + (op2*10) + (add1*100) + (op1*1000);
        printf("MBR : %d\n",mbr);

        ibr = op2*10 + add2;
        printf("IBR : %d\n",ibr);
        ir = op1;
        printf("IR  : %d\n",ir);
        mar = add1;
        printf("MAR : %d\n",mar);
    }
    else                    //Nous sommes dans la deuxième instruction de l'adresse mémoire
    {
        mbr = add2 + (op2*10) + (add1*100) + (op1*1000);
        printf("MBR : %d\n",mbr);
        
        ir = op2;
        printf("IR  : %d\n",ir);
        mar = add2;
        printf("MAR : %d\n",mar);
        pc = pc + 1;
        printf("PC  : %d\n",pc);
        ibr = 0;
        printf("IBR : %d\n",ibr);
    }

    switch (ir)             //Switch qui permet de déterminer l'instruction grâce à l'IR
    {
    case 1:
        //LOAD M(X)
        mbr = loadMX(mar);
        printf("MBR : %d\n",mbr);
        ac = mbr;
        printf("AC  : %d\n\n",ac);
        break;
    
    case 2:
        //LOAD -M(X)
        mbr = loadMX(mar);
        printf("MBR : %d\n",mbr);
        ac = (-1) * mbr;
        printf("AC  : %d\n\n",ac);
        break;

    case 3:
        //LOAD |M(X)|
        mbr = loadMX(mar);
        printf("MBR : %d\n",mbr);
        ac = abs(mbr);
        printf("AC  : %d\n\n",ac);
        break;

    case 4:
        //LOAD -|M(X)|
        mbr = loadMX(mar);
        printf("MBR : %d\n",mbr);
        ac = (-1) * abs(mbr);
        printf("AC  : %d\n\n",ac);
        break;

    case 5:
        //ADD M(X)
        mbr = loadMX(mar);
        printf("MBR : %d\n",mbr);
        ac = ac + mbr;
        printf("AC  : %d\n\n",ac);
        break;

    case 6:
        //SUB M(X)
        mbr = loadMX(mar);
        printf("MBR : %d\n",mbr);
        ac = ac - mbr;
        printf("AC  : %d\n\n",ac);
        break;

    case 7:
        //ADD |M(X)|
        mbr = loadMX(mar);
        printf("MBR : %d\n",mbr);
        ac = ac + abs(mbr);
        printf("AC  : %d\n\n",ac);
        break;

    case 8:
        //SUB |M(X)|
        mbr = loadMX(mar);
        printf("MBR : %d\n",mbr);
        ac = ac - abs(mbr);
        printf("AC  : %d\n\n",ac);
        break;

    case 9:
        //LOAD MQ,M(X)
        mbr = loadMX(mar);
        printf("MBR : %d\n",mbr);
        mq = mbr;
        printf("MQ  : %d\n\n",mq);
        break;

    case 10:
        //LOAD MQ
        ac = mq;
        printf("AC  : %d\n",ac);
        mq = 0;
        printf("MQ  : %d\n\n",mq);
        break;

    case 11:
        //MUL M(X)
        mbr = loadMX(mar);
        printf("MBR : %d\n",mbr);
        printf("MQ  : %d\n",mq);
        ac = mq * mbr;
        printf("AC  : %d\n\n",ac);
        break;

    case 12:
        //DIV M(X)
        mbr = loadMX(mar);
        printf("MBR : %d\n",mbr);
        printf("AC  : %d\n",ac);
        mq = ac / mbr;
        printf("MQ  : %d\n",mq);
        ac = ac % mbr;
        printf("AC  : %d\n\n",ac);
        break;

    case 13:
        //JUMP M(X,0:19)
        ibr = 0;
        pc = mar;
        printf("PC  : %d\n\n", pc);
        break;

    case 14:
        //JUMP M(X,20:39)
        ibr = 1;
        pc = mar;
        printf("PC  : %d\n\n", pc);
        break;

    case 15:
        //JUMP+ M(X,0:19)
        if(ac<0)
        {
        ibr = 0;
        pc = mar;
        printf("PC  : %d\n\n", pc);
        }
        break;

    case 16:
        //JUMP+ M(X,20:39)
        if(ac<0)
        {
        ibr = 1;
        pc = mar;
        printf("PC  : %d\n\n", pc);
        }
        break;

    case 18:
        //STOR M(X,8:19)
        storAdd(ac, mar, t2d, 19);
        break;

    case 19:
        //STOR M(X,28:39)
        storAdd(ac, mar, t2d, 39);
        break;

    case 20:
        //LSH
        ac = ac * 2;
        printf("AC  : %d\n\n",ac);
        break;

    case 21:
        //RSH
        ac = ac / 2;
        printf("AC  : %d\n\n",ac);
        break;

    case 33:
        //STOR M(X)
        stor(ac,mar,t2d);
        break;

    default:
        //valeur
        valeur = BtoD(tab,0,40);
        printf("Valeur : %d\n\n", valeur);  
        break;
    }

    fclose(fichier);                        //Fermeture du fichier test.txt

    printMem(t2d,size);                     //Affichage de la memoire dans le terminal

    cycleRec(ibr,pc, ac, mq,t2d,size);      //Appel récursif de la fonction 
}
