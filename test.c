#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <math.h>

//======================= Fonction qui permet de passer d'un nombre binaire à un nombre décimal =====================

int BtoD(int tab[], int n)
{
    int entier = 0, i, j=0;

    for(i=n-1;i>0;i--)
    {
        entier = entier + tab[i]*pow(2,j);
        j++;
    }
    if(n==40 && tab[0]==0)
    {
        entier = entier*(-1);
    }
    return entier;
}

//======================= Fonction LOAD MQ ================================

int loadMQ(int mq)
{
    int res;
    res = mq;
    return res;
}

//======================= Fonction LOAD M(X) ================================

int loadMX(int *tab, int n)      
{
    int valeur, adresse, i, m, c, j;
    int t[40];
    FILE* fic = NULL;
    fic = fopen("test.txt","r");

    if (fic == NULL)
    {
        printf("Impossible d'ouvrir le fichier \n");
        exit(0);
    }

    adresse = BtoD(tab,12);

    i = (adresse*40)+adresse;

    fseek(fic,i,SEEK_SET);

    for(j=0; j<40;j++)
    {
        c = fgetc(fic);         
        t[j] = c-48;
    }
    valeur = BtoD(t,40);

    return valeur;
}

//======================= Fonction LOAD -M(X) ================================

int loadNegMX(int tab[], int n)
{
    int valeur;
    valeur = loadMX(tab,12);
    valeur = valeur * (-1);
    return valeur;
}

//======================= Fonction LOAD |M(X)| ================================

int loadAbsMX(int tab[], int n)
{
    int valeur;
    valeur = loadMX(tab,12);
    valeur = abs(valeur);
    return valeur;
}

//======================= Fonction LOAD -|M(X)| ================================

int loadNegAbsMX(int tab[], int n)
{
    int valeur;
    valeur = loadMX(tab,12);
    valeur = abs(valeur);
    valeur = valeur * (-1);
    return valeur;
}
//===============================================================================================================

void test ()
{
    // ========================  Initialisation des variables et ouverture du fichier  =======================

    int i,c, k = 1, j,pc =1, mq, ac, valeur;
    int tab[40], mbr[40], ibr[20], mar[12], ir[8];

    FILE* fichier = NULL;
    fichier = fopen("test.txt","r");

    if (fichier == NULL)
    {
        printf("Impossible d'ouvrir le fichier \n");
        exit(0);
    }

    //==========================  Affichage du MBR , du PC et du IBR ================================

    printf("\nPC  : %d\n",pc);      // affichage du PC

    for(i = 0; i<40;i++)
    {
        c = fgetc(fichier);
        tab[i] = c-48;              
        mbr[i] = tab[i];            // remplissage du tableau MBR

        if(i>19)
        {
            ibr[i-20] = tab[i];     // remplissage du tableau IBR
        }
        if(i<8)
        {
            ir[i] = tab[i];         // remplissage du tableau IR
        }
        else
        {
            mar[i-8] = tab[i];      // remplissage du tableau MAR
        }
    }
    printf("\nMBR : ");

    for(i=0;i<40;i++)
        printf("%d",mbr[i]);        // affichage du MBR

    printf("\n\nIBR : ");

    for(i=0;i<20;i++)
        printf("%d",ibr[i]);        // affichage du IBR

    printf("\n\nIR  : ");

    for(i=0;i<8;i++)
        printf("%d",ir[i]);         // affichage du IR

    printf("\n\nMAR : ");
    
    for(i=0;i<12;i++)
        printf("%d",mar[i]);        // affihage de MAR

    
    j = 0;

    // faire une fonction a par pour trouver a quelle instruction nous avons a faire 
    // qui prend en paramètre le tab[40] et qui retourne l'instruction correspondante
    // OU ALORS qui appelle la fonction correspondante a l'instruction qui modifie les
    // valeurs des IBR, IR, MBR etc..
    
    switch (k)
    {
    case 1:
    {
        int t[8] = {0,0,0,0,1,0,1,0};
        while(j<8 && tab[j] == t[j])
        {
            j++;
        }
        if(j == 8){
            ac = loadMQ(mq);
            mq = 0;
            break;    
        }
        else{
            j = 0;
            k++;
        }
    }

    case 2:
    {
        int t[8] = {0,0,0,0,1,0,0,1};
        while(j<8 && tab[j] == t[j])
        {
            j++;
        }
        if(j == 8){
            mq = loadMX(mar,12);
            printf("%d \n",k);
            break;    
        }
        else{
            j=0;
            k++;
        }
    }

    case 3:
    {
        int t[8] = {0,0,1,0,0,0,0,1};
        while(j<8 && tab[j] == t[j])
        {
            j++;
        }
        if(j == 8){
            //storMX();                 // Trouver comment faire pour transcrire de decimal en binaire
                                        // et ouvrir le ficher et ecrire en décalant avec fseek.
            printf("%d \n",k);
            break;    
        }
        else{
            j=0;
            k++;
        }
    }

    case 4:
    {
        int t[8] = {0,0,0,0,0,0,0,1};
        while(j<8 && tab[j] == t[j])
        {
            j++;
        }
        if(j == 8){
            ac = loadMX(mar,12);
            printf("\n\nMBR : %d", ac);
            printf("\n\nAC  : %d", ac);
            break;    
        }
        else{
            j=0;
            k++;
        }
    }

    case 5:
    {
        int t[8] = {0,0,0,0,0,0,1,0};
        while(j<8 && tab[j] == t[j])
        {
            j++;
        }
        if(j == 8){
            ac = loadNegMX(mar,12);
            break;    
        }
        else{
            j=0;
            k++;
        }
    }

    case 6:
    {
        int t[8] = {0,0,0,0,0,0,1,1};
        while(j<8 && tab[j] == t[j])
        {
            j++;
        }
        if(j == 8){
            ac = loadAbsMX(mar,12);
            break;    
        }
        else{
            j=0;
            k++;
        }
    }

    case 7:
    {
        int t[8] = {0,0,0,0,0,1,0,0};
        while(j<8 && tab[j] == t[j])
        {
            j++;
        }
        if(j == 8){
            ac = loadNegAbsMX(mar,12);
            break;    
        }
        else{
            j=0;
            k++;
        }
    }

    case 8:
    {
        int t[8] = {0,0,0,0,1,1,0,1};
        while(j<8 && tab[j] == t[j])
        {
            j++;
        }
        if(j == 8){
            //jumpMX019();
            printf("%d",k);
            break;    
        }
        else{
            j=0;
            k++;
        }
    }

    case 9:
    {
        int t[8] = {0,0,0,0,1,1,1,0};
        while(j<8 && tab[j] == t[j])
        {
            j++;
        }
        if(j == 8){
            //jumpMX2039();
            printf("%d",k);
            break;    
        }
        else{
            j=0;
            k++;
        }
    }

    case 10:
    {
        int t[8] = {0,0,0,0,1,1,1,1};
        while(j<8 && tab[j] == t[j])
        {
            j++;
        }
        if(j == 8){
            //jumpPlusMX019();
            printf("%d",k);
            break;    
        }
        else{
            j=0;
            k++;
        }
    }

    case 11:
    {
        int t[8] = {0,0,0,1,0,0,0,0};
        while(j<8 && tab[j] == t[j])
        {
            j++;
        }
        if(j == 8){
            //jumpPlusMX2039();
            printf("%d",k);
            break;    
        }
        else{
            j=0;
            k++;
        }
    }

    case 12:
    {
        int t[8] = {0,0,0,0,0,1,0,1};
        while(j<8 && tab[j] == t[j])
        {
            j++;
        }
        if(j == 8){
            //addMX();
            printf("%d",k);
            break;    
        }
        else{
            j=0;
            k++;
        }
    }

    case 13:
    {
        int t[8] = {0,0,0,0,0,1,1,1};
        while(j<8 && tab[j] == t[j])
        {
            j++;
        }
        if(j == 8){
            //addAbsMX();
            printf("%d",k);
            break;    
        }
        else{
            j=0;
            k++;
        }
    }

    case 14:
    {
        int t[8] = {0,0,0,0,0,1,1,0};
        while(j<8 && tab[j] == t[j])
        {
            j++;
        }
        if(j == 8){
            //subMX();
            printf("%d",k);
            break;    
        }
        else{
            j=0;
            k++;
        }
    }

    case 15:
    {
        int t[8] = {0,0,0,0,1,0,0,0};
        while(j<8 && tab[j] == t[j])
        {
            j++;
        }
        if(j == 8){
            //subAbsMX();
            printf("%d",k);
            break;    
        }
        else{
            j=0;
            k++;
        }
    }

    case 16:
    {
        int t[8] = {0,0,0,0,1,0,1,1};
        while(j<8 && tab[j] == t[j])
        {
            j++;
        }
        if(j == 8){
            //mulMX();
            printf("%d",k);
            break;    
        }
        else{
            j=0;
            k++;
        }
    }

    case 17:
    {
        int t[8] = {0,0,0,0,1,1,0,0};
        while(j<8 && tab[j] == t[j])
        {
            j++;
        }
        if(j == 8){
            //divMX();
            printf("%d",k);
            break;    
        }
        else{
            j=0;
            k++;
        }
    }

    case 18:
    {
        int t[8] = {0,0,0,1,0,1,0,0};
        while(j<8 && tab[j] == t[j])
        {
            j++;
        }
        if(j == 8){
            //lsh();
            printf("%d",k);
            break;    
        }
        else{
            j=0;
            k++;
        }
    }

    case 19:
    {
        int t[8] = {0,0,0,1,0,1,0,1};
        while(j<8 && tab[j] == t[j])
        {
            j++;
        }
        if(j == 8){
            //rsh();
            printf("%d",k);
            break;    
        }
        else{
            j=0;
            k++;
        }
    }

    case 20:
    {
        int t[8] = {0,0,0,1,0,0,1,0};
        while(j<8 && tab[j] == t[j])
        {
            j++;
        }
        if(j == 8){
            //storMX819();
            printf("%d",k);
            break;    
        }
        else{
            j=0;
            k++;
        }
    }

    case 21:
    {
        int t[8] = {0,0,0,1,0,0,1,1};
        while(j<8 && tab[j] == t[j])
        {
            j++;
        }
        if(j == 8){
            //storMX2839();
            printf("%d",k);
            break;    
        }
        else{
            j=0;
            k++;
        }
    }

    default:
        valeur = BtoD(tab,40);
        printf("Nous avons à faire a une valeur : %d\n", valeur);
        break;
    }

    fclose(fichier);
}

int main()
{
    test();
    printf("\n");
    return 0; 
}