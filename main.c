#include "fonctions.h"

int main()
{
    int ibr = 0, pc = 0, ac = 0, mq = 0;
    int tab2D[4096][40];
    int c = 0,i,j=0;

    FILE* fichier = NULL;
    fichier = fopen("test.txt","r");    // ouverture du ficher test.txt en mode lecture

    if (fichier == NULL)
    {
        printf("Impossible d'ouvrir le fichier \n");
        exit(0);
    }

    while(c!=EOF)                       // Initialisation du tableau représentant la mémoire
    {
        for(i = 0; i<40;i++)
        {
            c = fgetc(fichier);
            tab2D[j][i] = c-48;      
        }
        fseek(fichier,1,SEEK_CUR);
        j++;
    }
    
    cycleRec(ibr,pc, ac, mq,tab2D,j);   // Appel de la fonction recusive principale

    return 0;
}